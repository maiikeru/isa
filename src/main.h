/*
***
* Soubor: main.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/

#ifndef _MAIN_H
#define _MAIN_H

#include <iostream>
#include "parameter.h"
#include "server.h"

using namespace std;


#endif /* ! _MAIN_H */
