/*
***
* Soubor: packet.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/


#include "packet.h"


size_t Packet::getPacketSize() {
    return packet_size;
}

char Packet::getByte(int offset) {

    return this->dataP[offset];
}
/* Inspirace z projektu z pedmetu IJC*/
int Packet::getOpcode(int offset) {
    int hx = getByte(offset);
    int lx = getByte(offset + 1);

    return ((hx << 8) | lx);
}

string Packet::getFileName(int *offset) {
    string name = "";

    while (this->dataP[*offset] != '\0') {
        name += this->getByte(*offset);
        (*offset)++;
    }
    return name;
}

string Packet::getPacketMode(int *offset) {
    string mode = "";

    while (this->dataP[*offset] != '\0') {
        mode += this->getByte(*offset);
        (*offset)++;
    }
    return mode;
}

void Packet::clear() {

    this->packet_size = 0;
    memset(this->dataP, 0, this->lenghtPack);
}

bool Packet::addByte(char byte) {
    if (this->packet_size >= this->lenghtPack) {
        return false;
    }

    this->dataP[this->packet_size] = byte;
    this->packet_size++;

    return true;
}

bool Packet::addString(const char *src) {
    size_t lenght_str = strlen(src);

    unsigned int i;
    for (i = 0; i < lenght_str; i++) {
        if (!addByte(src[i])) {return false;}
    }

    return true;
}

bool Packet::addData(char *buffer) {

    string dest = (const char *) buffer;
    unsigned long  len = dest.length();

    if ((packet_size + len) >= this->lenghtPack) {
        cerr << "nelze zapsat na paket, data is too big" << endl;
        return false;
    }

    memcpy(&(this->dataP[this->packet_size]), buffer, len);

    this->packet_size += len;
    return true;
}

bool Packet::copyBuffer(char *src, size_t length) {


    memcpy(this->data, src, length);

    memset(this->dataP, 0, this->lenghtPack);
    memcpy(this->dataP, src, length);

    return true;
}

bool Packet::isRRQ() {
    return (this->getOpcode(0) == OPCODE_RRQ);
}

bool Packet::isWRQ() {
    return (this->getOpcode(0) == OPCODE_WRQ);
}

bool Packet::isData() {
    return (this->getOpcode(0) == OPCODE_DATA);
}

bool Packet::isACK() {
    return (this->getOpcode(0) == OPCODE_ACK);
}

bool Packet::isError() {
    return (this->getOpcode(0) == OPCODE_ERROR);
}

bool Packet::createACK(int blok) {
    this->clear();
    this->addByte(OPCODE_ZERO);
    this->addByte(OPCODE_ACK);
    this->addByte(OPCODE_ZERO);
    this->addByte(blok);
    return true;
}

bool Packet::createData(int blok, char *src) {
    this->clear();
    if (!this->addByte(OPCODE_ZERO)) {return false;}
    if (!this->addByte(this->op_code_DATA)) {return false;}
    if (!this->addByte(OPCODE_ZERO)) {return false;}
    if (!this->addByte(blok)) {return false;}
    if (!this->addData(src)) {return false;}
    return true;
}

bool Packet::createError(char const *src, int code) {
    this->clear();
    if (!this->addByte(OPCODE_ZERO)) {return false;}
    if (!this->addByte(this->op_code_ERR)) {return false;}
    if (!this->addByte(OPCODE_ZERO)) {return false;}
    this->addByte(code);
    this->addString(src);
    this->addByte(OPCODE_ZERO);

    return true;
}

char *Packet::getDataPack() {

    return &(this->dataP[4]);
}

size_t Packet::lenghtDataPack() {

    string data = this->getDataPack();
    return data.length();

}

int Packet::getNumBlock() {

    if (this->isACK() || this->isData())
        return this->getOpcode(2);


    return 0;
}


bool Packet::createOACK(string mode) {
    this->clear();
    this->addByte(OPCODE_ZERO);
    this->addByte(OPCODE_OACK);
    // this->addByte(OPCODE_ZERO);


    char portString[6];

    for (auto &j : this->optRank) {

        if (j == 1) {
            this->addString(this->optName_1);
            this->addByte(OPCODE_ZERO);
            sprintf(portString, "%u", this->optBlksize);
            this->addString((const char *) portString);
            this->addByte(OPCODE_ZERO);
        }
        else if (j == 2) {
            this->addString(this->optName_2);
            this->addByte(OPCODE_ZERO);
            sprintf(portString, "%u", this->optTimeout);
            this->addString((const char *) portString);
            this->addByte(OPCODE_ZERO);
        }
        else if ((j == 3) && (mode == "RRQ")) {
            this->addString(this->optName_3);
            this->addByte(OPCODE_ZERO);
            sprintf(portString, "%u", this->optTsize);
            this->addString((const char *) portString);
            this->addByte(OPCODE_ZERO);
        }
        else if ((j == 4) && (mode == "RRQ") && (this->optMulticastFlag)) {
            this->addString(this->optName_4);
            this->addByte(OPCODE_ZERO);

            this->addByte(OPCODE_ZERO);
        }
    }


    return true;
}


bool Packet::initArray(int num) {

    if ((this->dataP = (char *) malloc(num * sizeof(char))) == NULL) {
        cerr << "Nelze malokovat pamet0" << endl;
        return false;
    }

    this->lenghtPack = num;


    return true;
}

string Packet::getOption(int *offset) {
    string opt = "";
    while (this->dataP[*offset] != '\0') {
        opt += this->dataP[*offset];
        (*offset)++;
    }

    return opt;
}

bool Packet::storeOption(int *offset) {

    string tmp = "";
    int i = 0;

    memset(this->optRank, 0, 4);


    while (true) {
        tmp = this->getOption(&(*offset));
        (*offset)++;
        if (tmp.empty()) {

            if (i == 0) {
                return false;
            }

            break;
        }

        if (tmp == "blksize") {
            if (this->optBlksize == -1) {
                this->optRank[i] = 1;
                i++;
            }

            this->optBlksize = stoi(this->getOption(&(*offset)));
        }
        else if (tmp == "timeout") {
            if (this->optTimeout == -1) {
                this->optRank[i] = 2;
                i++;
            }

            this->optTimeout = stoi(this->getOption(&(*offset)));
        }
        else if (tmp == "tsize") {
            if (this->optTsize == -1) {
                this->optRank[i] = 3;
                i++;
            }

            this->optTsize = stoi(this->getOption(&(*offset)));
        }
        else if (tmp == "multicast") {
            if (!this->optMulticastFlag) {
                this->optRank[i] = 4;
                i++;
            }

            this->optMulticastFlag = true;
        }

        (*offset)++;
        tmp = "";
    }

    return true;
}

bool Packet::createDataEmpty(int blok) {

    this->clear();
    if (!this->addByte(this->op_code_0)) {return false;}
    if (!this->addByte(this->op_code_DATA)) {return false;}
    if (!this->addByte(this->op_code_0)) {return false;}
    if (!this->addByte(blok)) {return false;}


    return true;
}
