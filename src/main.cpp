/*
***
* Soubor: main.cpp
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis: TFTP Server, kterz bude obluhovat zadosti od klientu, a to jako IPv4 i IPv6
***
*/

#include "main.h"

int main(int argc, char const *argv[]) {

    // Inicializace tridy Parameter
    Parameter *prmt = new Parameter();

    // test na napovedu
    if (argc == 2 && (strcmp(argv[1], "-h") == 0)) {
        prmt->printHelp();
        delete(prmt);
        return true;
    }

    // Analyzuj parametry
    if (!prmt->processCommandArgs(argc, (char **) argv)) {
        delete(prmt);
        return EXIT_FAILURE;
    }

    // Nastav defaultne timeout na 3s, kdyz neni zadan
    if (prmt->timeout == -1)
        prmt->timeout = 3;

    // Vypis timout
    cout << "Max.timeout: " << prmt->timeout << endl;

    // Jestli,neni zadany parametr -a
    if (prmt->AAdres.empty()) {
        prmt->AAdres.push_back("127.0.0.1");
        prmt->port.push_back(69);
    }


    // Inicializace Serveru
    TFTPServer *server = new TFTPServer();

    if (!server->TFTPServerr(prmt)) {
        delete(prmt);
        delete(server->pack);
        delete(server);
        return EXIT_FAILURE;
    }

    delete(prmt);
    delete(server->pack);
    delete(server);

    return EXIT_SUCCESS;
}
