/*
***
* Soubor: server.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/


#include "server.h"


int TFTPServer::TFTPServerr(Parameter *prmt) {

    // ulozeni ukazatele na strukturu Parameter
    this->param = prmt;

    return this->ServerInit();
}

/*Server UDP inspirace ISA opora c 2*/
bool TFTPServer::ServerInit() {

    struct addrinfo hints, *servinfo;
    int retCode, serverPort;
    char const *serverAdres;
    char portString[6];
    pid_t pid;

    fd_set rfds;
    struct timeval tv;
    int retval;
    FD_ZERO(&rfds);

    // nastaveni struktury
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    // pouziti IPv4 nebo IPv6
    hints.ai_socktype = SOCK_DGRAM; // datagram socket
    hints.ai_flags = AI_PASSIVE;    // use my IP

    for (unsigned int i = 0; i < this->param->AAdres.size(); i++) {


        // ziskani portu a adresy
        serverAdres = this->param->AAdres.at(i).c_str();
        serverPort = this->param->port.at(i);

        //prevedeni int na char
        sprintf(portString, "%u", serverPort);

        if ((retCode = getaddrinfo(serverAdres, portString, &hints, &servinfo)) != 0) {
            cerr << "getaddrinfo: " << gai_strerror(retCode) << endl;
            return false;
        }

        // vytvoreni spojeni na danem socketu
        if (!this->createConnection(servinfo))
            return false;

        // uz neni potreba
        freeaddrinfo(servinfo);


        this->socketServer.push_back(this->sockfd);
        FD_SET(this->sockfd, &rfds);
    }

    tv.tv_sec = 1;
    tv.tv_usec = 0;

    /*Multiplexering inspirace ISA opora c 2*/
    for (; ;) {

        for (unsigned int i = 0; i < this->socketServer.size(); i++) {

            FD_SET(this->socketServer.at(i), &rfds);
            retval = select(this->socketServer.at(i) + 1, &rfds, NULL, NULL, &tv);

            if (retval < 0)
                perror("select()");
            else if (retval) {
                if ((pid = fork()) > 0) { //parent

                    int status;
                    waitpid(pid, &status, 0);
                }
                else if (pid < 0) { //error
                    cerr << "Server: fork()" << endl;

                    for (unsigned int j = 0; j < this->socketServer.size(); j++) {
                        close(this->socketServer.at(j));
                    }

                    return false;
                }
                else { //child

                    this->adresoun = this->param->AAdres.at(i).c_str();


                    if (!this->getNameMTU(this->socketServer.at(i))) {
                        close(this->socketServer.at(i));
                        return false;
                    }

                    this->printMTU();

                    // ziskani velikost rozhrani na kterem se komunikuje
                    this->sizeActiveMTU = this->getActiveMTU(this->socketServer.at(i), this->param->AAdres.at(i));

                    // MTU - (TFTP 4B + UDP 8B + IP 20B)
                    this->sizeActiveMTU = this->sizeActiveMTU - (4 + 8 + 20);
                    this->setSizeMTU();


                    if (!this->clientConection(this->socketServer.at(i), this->param->AAdres.at(i)))
                        break;

                }
            }

            FD_ZERO(&rfds);
        }
    }


    // uvolneni socketu
    close(this->sockfd);
    return true;
}

/*Inspirace ISA opora c 2*/
bool TFTPServer::createConnection(struct addrinfo *servinfo) {

    struct addrinfo *point;

    // vytvoreni spojeni na danem socketu
    for (point = servinfo; point != NULL; point = point->ai_next) {
        if ((this->sockfd = socket(point->ai_family, point->ai_socktype, point->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (bind(this->sockfd, point->ai_addr, point->ai_addrlen) == -1) {
            close(this->sockfd);
            perror("server: bind");
            continue;
        }
        break;
    }

    if (point == NULL) {
        close(this->sockfd);
        cerr << "server: failed to bind socket" << endl;
        return false;
    }

    return true;
}

bool TFTPServer::createConnectionSC(struct addrinfo *servinfo) {

    struct addrinfo *point;

    // vytvoreni spojeni na danem socketu
    for (point = servinfo; point != NULL; point = point->ai_next) {
        if ((this->socketSC = socket(point->ai_family, point->ai_socktype, point->ai_protocol)) == -1) {
            perror("server: socket");
            continue;
        }

        if (bind(this->socketSC, point->ai_addr, point->ai_addrlen) == -1) {
            close(this->socketSC);
            perror("server: bind");
            continue;
        }
        break;
    }

    if (point == NULL) {
        close(this->socketSC);
        cerr << "server: failed to bind socket" << endl;
        return false;
    }

    return true;
}

/*Inspirace http://www.linuxhowtos.org/manpages/3/getifaddrs.htm*/
bool TFTPServer::getNameMTU(int socket) {

    struct ifaddrs *ifaddr, *ifa;

    if (getifaddrs(&ifaddr) == -1) {
        perror("server: getifaddrs");
        return false;
    }

    char const *nameInterface = "";
    int valueMTU = 0;
    bool mtuFlag = 0;

    // ziskani nazvu pripojenych rozhrani
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL || nameInterface == ifa->ifa_name)
            continue;

        nameInterface = ifa->ifa_name;

        for (auto &i : mtuName) {
            if (i == nameInterface) {
                mtuFlag = 1;
                this->mtuName.pop_back();
                this->mtuValue.pop_back();
                break;
            }
        }

        if (mtuFlag == 0) {
            if ((valueMTU = this->getValueMTU(socket, nameInterface)) == -1)
                return false;

            this->mtuName.push_back(ifa->ifa_name);
            this->mtuValue.push_back(valueMTU);
        }
        mtuFlag = 0;
    }

    //uvolneni structury ifaddrs
    freeifaddrs(ifaddr);

    return true;
}

int TFTPServer::getValueMTU(int fd, const char *device) {
    struct ifreq ifr;

    if (!device)
        return 65535;

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, device, sizeof(ifr.ifr_name));

    if (ioctl(fd, SIOCGIFMTU, &ifr) == -1) {
        perror("server: Error opening:");
        return -1;
    }

    return ifr.ifr_mtu;
}

void TFTPServer::printMTU() {

    cout << "Max.blocksize: ";
    for (unsigned int i = 0; i < this->mtuName.size(); i++) {
        if (i == (this->mtuName.size() - 1))
            cout << this->mtuName[i] << "=" << this->mtuValue[i] << "B";
        else
            cout << this->mtuName[i] << "=" << this->mtuValue[i] << "B" << ", ";
    }
    cout << endl;

    // uvolneni pameti vektoru
    this->mtuName.clear();
    this->mtuValue.clear();
}


bool TFTPServer::clientConection(int fd, string srcAdress) {
    ssize_t numBytes;
    int offset, retCode;

    //char buffak2[1024];
    char portString[6];
    pid_t pid;

    this->setTimeout();

    // Inicializuj pole na packety
    this->pack->initArray(this->sizeActiveMTU);

    // prijem zpravymake
    this->addr_len = sizeof(this->client_addr);
    numBytes = recvfrom(fd, this->pack->dataP, this->serverBlksize, 0, (struct sockaddr *) &this->client_addr, &this->addr_len);

    this->pack->dataP[numBytes] = '\0';

    if (numBytes == 1) {
        cerr << "Server: recvfrom" << endl;
        return false;
    }
    else if (numBytes <= 0) {
        cerr << "Server: recvfrom chyba" << endl;
        return false;
    }


/*
    if (!this->pack->copyBuffer(buffak, numBytes++)) {
        free(this->pack->dataP);
        return false;
    }
*/
    if (this->pack->isRRQ()) {

        if ((pid = fork()) > 0) { /* parent */

            int status;
            waitpid(pid, &status, 0);
        }
        else if (pid < 0) { /* error*/
            cerr << "Server: fork()" << endl;
            free(this->pack->dataP);
            return false;
        }
        else { /* child */

            offset = 2;
            this->packFileName = this->pack->getFileName(&offset);
            this->printDialog("requested READ of " + this->packFileName);

            offset++;
            this->packFileMode = this->pack->getPacketMode(&offset);

            // test na extetdet option a pripadne nacteni
            offset++;
            this->extendetOption = pack->storeOption(&offset);

            struct addrinfo hints2, *servinfo2;
            memset(&hints2, 0, sizeof hints2);
            hints2.ai_family = AF_UNSPEC;
            hints2.ai_socktype = SOCK_DGRAM;
            hints2.ai_flags = AI_PASSIVE;

            sprintf(portString, "%u", this->generatePort());

            if ((retCode = getaddrinfo(srcAdress.c_str(), portString, &hints2, &servinfo2)) != 0) {
                cerr << "getaddrinfo: " << gai_strerror(retCode) << endl;
                return 1;
            }
            // vytvoreni spojeni na danem socketu
            if (!this->createConnectionSC(servinfo2)) {
                free(this->pack->dataP);
                return false;
            }

            // uz neni potreba
            freeaddrinfo(servinfo2);

            this->blackBoxRRQ(this->socketSC);

            free(this->pack->dataP);
            fclose(this->fr);
            close(this->socketSC);
        }

    }
    else if (this->pack->isWRQ()) {
        if ((pid = fork()) > 0) { /* parent */

            int status;
            waitpid(pid, &status, 0);
        }
        else if (pid < 0) { /* error*/
            cerr << "Server: fork()" << endl;
            free(this->pack->dataP);
            return false;
        }
        else { /* child */

            offset = 2;
            this->packFileName = this->pack->getFileName(&offset);
            this->printDialog("requested WRITE of " + this->packFileName);

            offset++;
            this->packFileMode = this->pack->getPacketMode(&offset);

            // test na extetdet option a pripadne nacteni
            offset++;
            this->extendetOption = this->pack->storeOption(&offset);

            struct addrinfo hints2, *servinfo2;
            memset(&hints2, 0, sizeof hints2);
            hints2.ai_family = AF_UNSPEC;
            hints2.ai_socktype = SOCK_DGRAM;
            hints2.ai_flags = AI_PASSIVE;

            sprintf(portString, "%u", this->generatePort());

            if ((retCode = getaddrinfo(srcAdress.c_str(), portString, &hints2, &servinfo2)) != 0) {
                cerr << "getaddrinfo: " << gai_strerror(retCode) << endl;
                return 1;
            }
            // vytvoreni spojeni na danem socketu
            if (!this->createConnectionSC(servinfo2)) {
                free(this->pack->dataP);
                return false;
            }

            // uz neni potreba
            freeaddrinfo(servinfo2);


            this->blackBoxWRQ(this->socketSC);

            free(this->pack->dataP);
            fclose(this->fr);
            close(this->socketSC);

        }
    }
    else {
        /*++++++++++++++++++++++++++/*/
        cerr << " neni to rrr a niqwqw" << endl;
    }


    return true;
}

bool TFTPServer::sendPacket() {

    this->addr_len = sizeof(this->client_addr);

    ssize_t send = sendto(this->socketSC, this->pack->dataP, this->pack->getPacketSize(), 0,
            (struct sockaddr *) &this->client_addr, this->addr_len);

    if (send == -1)
        cerr << "sendto" << endl;

    return true;
}


void TFTPServer::printDialog(string src) {
    string output = "";

    output += this->getTime();
    output += " ";
    output += this->adresoun;
    output += " ";
    output += src;

    cout << output << endl;
}

int TFTPServer::generatePort() {

    srand(time(NULL));
    int num = rand() % (USHRT_MAX - 1000) + 1000;

    return num;
}

string TFTPServer::getTime() {
    time_t t = time(NULL);
    char buff_time[100];
    strftime(buff_time, sizeof(buff_time), "[%F %T]", localtime(&t));
    return buff_time;
}

bool TFTPServer::isFileExist(string file_name, string mode) {

    string soubor = this->param->path + file_name;
    string errMsg = "sending ERROR <";
    const char *modes;

    if (this->packFileMode != "octet") {
        if (this->packFileMode != "netascii") {
            this->pack->createError(this->pack->err_Msg_2, this->pack->err_code_2);
            errMsg += this->pack->err_Msg_2;
            this->printDialog(errMsg + ">");
            return false;
        }
    }

    if (mode == "RRQ") {
        if (this->packFileMode == "octet")
            modes = "rb";
        else
            modes = "r";
    }
    else {
        if (this->packFileMode == "octet")
            modes = "wb";
        else
            modes = "w";
    }

    cout<<this->fr<<endl;
    if ((this->fr = fopen(soubor.c_str(), modes)) == NULL) {

        if (errno == ENOENT) {
            this->pack->createError(this->pack->err_Msg_1, this->pack->err_code_1);
            errMsg += this->pack->err_Msg_1;
            this->printDialog(errMsg + ">");
            return false;
        }
        else if (errno == EACCES) {
            this->pack->createError(this->pack->err_Msg_2, this->pack->err_code_2);
            errMsg += this->pack->err_Msg_2;
            this->printDialog(errMsg + ">");

            return false;
        }
        else if (errno == ENOMEM) {
            this->pack->createError(this->pack->err_Msg_3, this->pack->err_code_3);
            errMsg += this->pack->err_Msg_3;
            this->printDialog(errMsg + ">");
            return false;
        }
        else if (errno == EEXIST) {
            this->pack->createError(this->pack->err_Msg_6, this->pack->err_code_6);
            errMsg += this->pack->err_Msg_6;
            this->printDialog(errMsg + ">");
            return false;
        }
        else if (errno == ENFILE) {
            this->pack->createError(this->pack->err_Msg_3, this->pack->err_code_3);
            errMsg += this->pack->err_Msg_3;
            this->printDialog(errMsg + ">");
            return false;
        }
    }
    cout<<this->fr<<endl;

    fseek(this->fr, 0, SEEK_END);
    this->lSize = ftell(this->fr);
    cout<<ftell(this->fr)<<endl;

    return true;
}


char *TFTPServer::getFileBlock(int size_block) {

    if ((this->buffer = (char *) malloc(size_block * sizeof(char))) == NULL) {
        cerr << "Nelze malokovat pamet2" << endl;
        return NULL;
    }

    memset(this->buffer, 0, size_block * sizeof(char));

    if (!this->flagFile) {

        rewind(this->fr);

        this->ret_code = fread(this->buffer, 1, size_block * sizeof(char), this->fr);
/*
        if (this->ret_code < 0) {
            cout << "fseek chyba" << endl;
            return NULL;

        }
        */
        this->fseek_count += this->ret_code;
        this->flagFile = true;
    }
    else {
        fseek(this->fr, this->fseek_count, SEEK_SET);

        this->ret_code = fread(buffer, 1, size_block * sizeof(char), this->fr);


/*
        if (this->ret_code < 0) {
            return NULL;
        }
*/
        this->fseek_count += this->ret_code;
    }

    return (this->buffer);
}

bool TFTPServer::blackBoxRRQ(int fd) {

    string errMsg = "sending ERROR <";
    unsigned int blockCount = 1;
    int temp = 65;
    int line = 0;
    int sizeFile = 0;
    int readBlock = 0;
    unsigned int erer;
    int state = 1;
    int countTimer = 0;
    for (; ;) {

        switch (state) {
            // INIT
            case 1: {
                if (!this->isFileExist(this->packFileName, "RRQ")) {
                    this->sendPacket();
                    return false;
                }
                this->ret_code = 0;
                this->fseek_count = 0;
                this->pack->clear();
                sizeFile = this->lSize;
                cout<<sizeFile<<endl;
                rewind(this->fr);
            }
                // test jestli soubor neni prilis velky
            case 2: {
                if (!this->countBlock()) {
                    this->sendPacket();
                    return false;
                }
            }
                // test a nastaveni extension
            case 3: {
                if (this->extendetOption) {
                    this->settingOption();
                    this->settingOptionRRQ();
                }
                else {
                    state = 10;
                    break;
                }
            }
            case 4: {
                this->pack->createOACK("RRQ");
                this->printDialog("sending OACK");
                this->sendPacket();
                this->receivePacket(fd);

                if (!this->pack->isACK())
                    return false;

                this->printDialog("receiving ACK");
            }

            case 5: {
/*
                cout << "**" << endl;
                cout << this->pack->optBlksize << endl;
                cout << this->pack->optTimeout << endl;
                cout << this->pack->optTsize << endl;
                cout << this->pack->optMulticastFlag << endl;
                cout << "**" << endl;
*/
                state = 10;
            }

                //  mode file
            case 10: {
                this->pack->clear();

                if (this->packFileMode == "netascii") {
                    readBlock = 11;

                    if ((this->buffer = (char *) malloc(this->serverBlksize * sizeof(char))) == NULL) {
                        cerr << "Nelze malokovat pamet4" << endl;
                        return NULL;
                    }
                }
                else {
                    readBlock = 12;
                    state = 12;
                    break;
                }
            }
                // netascii
            case 11: {
                this->blockData = this->readAsciiBlock(this->serverBlksize, &temp, &line);
                state = 13;
                break;
            }
                //octet
            case 12: {
                this->blockData = this->getFileBlock(this->serverBlksize);
                state++;
            }
                // test count blok
            case 13: {
                if (blockCount > 0xFFFFU) {
                    this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                    errMsg += this->pack->err_Msg_4;
                    state = 55555;
                    break;
                }
            }
                // create data packet
            case 14: {
                if (!this->pack->createData(blockCount, this->blockData)) {
                    this->pack->clear();
                    this->pack->createError(this->pack->err_Msg_3, this->pack->err_code_3);
                    errMsg += this->pack->err_Msg_3;
                    state = 55555;
                    break;
                }
            }
                // send packet
            case 15: {
                this->sendPacket();
                this->printDialog("sending DATA of " + this->packFileName + " at block " + to_string(blockCount));
            }
                //receive packet
            case 16: {
                if (!this->receivePacket(fd)) {
                    this->pack->createError(this->pack->err_Msg_0, this->pack->err_code_0);
                    this->sendPacket();
                    errMsg += this->pack->err_Msg_0;
                    state = 55555;
                    break;
                }
            }
                // timer
            case 17: {
                if (this->timerFlag) {
                    this->printDialog("Timeout off");

                    this->timerFlag = false;
                    countTimer++;
                    state = 15;

                    if (countTimer == 3)
                    {
                        state = 77777;

                    }


                    break;
                }
            }
                //  is not ack
            case 18: {
                if (!this->pack->isACK()) {
                    if (this->pack->isError()) {
                        this->printDialog("receiving ERROR");
                        this->pack->clear();
                        return false;
                    }
                    else {
                        this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                        this->sendPacket();
                        errMsg += this->pack->err_Msg_4;
                        state = 5555;
                        break;
                    }
                }
            }
                // test ack
            case 19: {
                erer = this->pack->getNumBlock();
                if (erer != blockCount) {

                    if (erer == (blockCount -1))
                    {
                        fseek(this->fr, this->fseek_count - this->ret_code , SEEK_SET);
                        blockCount -= 1;
                        state = readBlock;
                        break;
                    }

                    this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                    this->sendPacket();
                    errMsg += this->pack->err_Msg_4;
                    state = 5555;
                    break;
                }

                this->printDialog("receiving ACK at block " + to_string(blockCount));
                blockCount++;
            }
                //test jestli soubor prenesen
            case 20: {
                sizeFile -= this->ret_code;

                if (sizeFile > 0) {
                    state = readBlock;
                    break;
                }
            }
                // osetreni, kdyz cteny soubor ma velikost jako prenesene oktety
            case 21: {
                if (this->ret_code == this->serverBlksize) {
                    this->ret_code = 0;
                    state = 22;
                    break;
                }
                // prenos ukoncen
                state = 77777;
                this->printDialog("File transfer is finished");
                break;
            }
                // vytvorim prazdny datapacket
            case 22: {
                if (blockCount > 0xFFFFU) {
                    this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                    errMsg += this->pack->err_Msg_4;
                    state = 55555;
                    break;
                }

                if (!this->pack->createDataEmpty(blockCount)) {
                    this->pack->clear();
                    this->pack->createError(this->pack->err_Msg_3, this->pack->err_code_3);
                    errMsg += this->pack->err_Msg_3;
                    state = 55555;
                    break;
                }
                state = 15;
                break;
            }
                // error a konec
            case 55555: {

                this->sendPacket();
                this->printDialog(errMsg + ">");
                this->pack->clear();
                return false;
            }

            default:
                break;
        }

        if (state == 77777) {
            this->pack->clear();
            break;
        }
    }

    if (this->packFileMode == "netascii") {
        free(this->buffer);
    }


    this->pack->clear();
    return true;
}

bool TFTPServer::blackBoxWRQ(int fd) {

    string errMsg = "sending ERROR <";
    bool testOack = false;

    unsigned int blockCount = 0;
    unsigned int test;
    int temp = 65;

    int state = 1;

    for (; ;) {
        switch (state) {
            //INIT
            case 1: {
                if (!this->isFileExist(this->packFileName, "WRQ")) {
                    this->sendPacket();
                    return false;
                }

                this->ret_code = 0;
                this->fseek_count = 0;
                this->pack->clear();
                rewind(this->fr);


            }
                // test extension
            case 2: {
                if (this->extendetOption) {
                    this->settingOption();
                }
                else {
                    state = 10;
                    break;
                }
            }
                // create OACK
            case 3: {
                this->pack->createOACK("WRQ");
                this->printDialog("sending OACK");
                this->sendPacket();
                blockCount++;

                state = 11;
                break;
            }
                // posli ack 0 kdyz neni OACK
            case 10: {
                this->pack->clear();

                this->pack->createACK(blockCount);
                this->sendPacket();
                this->printDialog("sending ACK at block " + to_string(blockCount));
                blockCount++;
            }
                // test na pocet bloku
            case 11: {
                if (blockCount > 0xFFFFU) {
                    this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                    this->sendPacket();
                    errMsg += this->pack->err_Msg_4;
                    this->printDialog(errMsg + ">");
                    this->pack->clear();
                    return false;
                }
            }

                //receive packet
            case 12: {
                if (!this->receivePacket(fd)) {
                    this->pack->createError(this->pack->err_Msg_0, this->pack->err_code_0);
                    this->sendPacket();
                    errMsg += this->pack->err_Msg_0;
                    state = 55;
                    break;
                }

                if (!testOack && this->extendetOption) {
                    testOack = true;
                    test = this->numbytes;
                    if (test != this->serverBlksize) {
                        this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                       // cout << "2" << endl;
                        this->sendPacket();
                        errMsg += this->pack->err_Msg_4;
                        state = 55;
                        break;
                    }
                }
            }
                // timer
            case 13: {
                if (this->timerFlag) {
                    this->printDialog("Timeout off");
                    state = 77;
                    this->timerFlag = false;
                    break;
                }
            }
                // je to data packet ?
            case 14: {
                if (!this->pack->isData()) {
                    if (this->pack->isError()) {
                        this->printDialog("receiving ERROR");
                        this->pack->clear();
                        return false;
                    }
                    else {
                        this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                        this->sendPacket();
                        errMsg += this->pack->err_Msg_4;
                        state = 55;
                        break;
                    }
                }
            }
                // test data packetu na blok
            case 15: {
                test = this->pack->getNumBlock();
                if (test != blockCount) {
                    this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
                    this->sendPacket();
                    errMsg += this->pack->err_Msg_4;
                    state = 55;
                    break;
                }

                this->printDialog("receiving DATA at block " + to_string(blockCount));
            }
                // write data
            case 16: {
                if (this->packFileMode == "netascii") {
                    this->writeAsciiBlock(this->pack->getDataPack(), this->pack->lenghtDataPack(), &temp);
                }
                else
                    this->writeFileBlock(this->pack->getDataPack(), this->pack->lenghtDataPack());
            }
                // create ack packet
            case 17: {
                if (!this->pack->createACK(blockCount)) {
                    this->pack->clear();
                    this->pack->createError(this->pack->err_Msg_3, this->pack->err_code_3);
                    errMsg += this->pack->err_Msg_3;
                    state = 55;
                    break;
                }
            }
                // posli  ack
            case 18: {
                this->sendPacket();
                this->printDialog("sending ACK at block " + to_string(blockCount));
                blockCount++;
            }
                // prisel cely soubor?
            case 19: {
                if (this->ret_code < this->serverBlksize) {
                    state = 77;
                    this->printDialog("File transfer is finished");
                    break;
                }

                state = 11;
                break;
            }
                // chyby
            case 55: {

                this->sendPacket();
                this->printDialog(errMsg + ">");
                this->pack->clear();
                return false;
            }

            default:
                break;
        }

        if (state == 77) {
            this->pack->clear();
            break;
        }
    }

    this->pack->clear();
    return true;
}

bool TFTPServer::writeFileBlock(char *src, size_t size) {

    this->ret_code = fwrite(src, sizeof(char), size, this->fr);

    return true;
}


bool TFTPServer::receivePacket(int fd) {
    int numBytes = 0;

    fd_set rfds;
    struct timeval tv;
    int retval;

    int countTimeout = 0;

    string timeMsg = "<No data within ";

    while (countTimeout < 3) {

        FD_ZERO(&rfds);
        FD_SET(fd, &rfds);

        tv.tv_sec = this->serverTimeout;
        tv.tv_usec = 0;

        retval = select(fd + 1, &rfds, NULL, NULL, &tv);

        if (retval == -1) {
            perror("select()");
            free(this->pack->dataP);
            return false;
        }
        else if (retval) {

            this->addr_len = sizeof(this->client_addr);

            numBytes = recvfrom(fd, this->pack->dataP, this->serverBlksize * sizeof(char) + 32, 0, (struct sockaddr *) &this->client_addr, &this->addr_len);
            this->pack->dataP[numBytes] = '\0';

           // cout << "Received<" << numBytes << endl;//delete

/*
            if (!this->pack->copyBuffer(receiveBuffer, numBytes + 1)) {
                free(receiveBuffer);
                return false;
            }
*/
            break;
        }

        else {
            this->printDialog(timeMsg + to_string(this->serverTimeout) + ">");
        }

        countTimeout++;

    }

    if (countTimeout == 3)
        this->timerFlag = true;

    this->numbytes = numBytes - 4;


    return true;
}


bool TFTPServer::writeAsciiBlock(char *src, size_t size, int *lastChar) {

    unsigned int i;
    int c;
    char prevchar = *lastChar;


    for (i = 0; i < size; i++) {
        c = src[i];
        if (prevchar == '\r') {
            if (c == '\n') {// crlf to lf

                fseek(this->fr, -1, SEEK_CUR);
                if (fputc(c, this->fr) == EOF)
                    break;
            }
                // crnul to cr //
            else if (c != '\0') {
                if (fputc(c, this->fr) == EOF)
                    break;
            }
        }
        else {
            if (fputc(c, this->fr) == EOF)
                break;
        }
        prevchar = c;
    }

    this->ret_code = i;
    *lastChar = prevchar;

    return true;
}

char *TFTPServer::readAsciiBlock(size_t size, int *lastChar, int *newline) {

    unsigned int i;
    unsigned int j = 0;
    int c;
    char prevchar = 'c';

    int tmp = *newline;

    memset(this->buffer, 0, size * sizeof(char));

    for (i = 0; i < size; i++) {

        if (tmp) {
            if (prevchar == '\n') {
                // lf to cr,lf //
                c = '\n';
                j++;
            }
            else {
                // cr to cr,nul //
                c = '\0';
                j++;
            }

            tmp = 0;
        }
        else {
            c = fgetc(this->fr);
            if (c == EOF)
                break;
            if (c == '\n' || c == '\r') {
                prevchar = c;
                c = '\r';
                tmp = 1;
            }
        }
        this->buffer[i] = c;

        j++;
    }

    *lastChar =  prevchar;
    *newline = tmp;
    this->ret_code = i;
    this->fseek_count += i;

    return (this->buffer);
}

int TFTPServer::getActiveMTU(int socket, string adresa) {


    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char host[NI_MAXHOST];
    struct ifreq ifr;
    ifr.ifr_addr.sa_family = AF_UNSPEC;

    if (getifaddrs(&ifaddr) == -1) {
        perror("server: getifaddrs");
        return false;
    }

    char const *nameInterface = "";
    int valueMTU = 0;

    // ziskani nazvu pripojenych rozhrani
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL || nameInterface == ifa->ifa_name)
            continue;

        nameInterface = ifa->ifa_name;
        family = ifa->ifa_addr->sa_family;


        if (family == AF_INET || family == AF_INET6) {
            s = getnameinfo(ifa->ifa_addr, (family == AF_INET) ? sizeof(struct sockaddr_in) : \
                sizeof(struct sockaddr_in6), host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        }

        /*if (s != 0) {
            printf("getnameinfo() failed: %s\n", gai_strerror(s));
            return false;
        }
*/
        if (host == adresa) {
            strcpy(ifr.ifr_name, nameInterface);
            if (ioctl(socket, SIOCGIFMTU, (caddr_t) &ifr) < 0)
                cerr << "ioctl" << endl;
            else
                valueMTU = ifr.ifr_mtu;

            break;
        }
    }

    //uvolneni structury ifaddrs
    freeifaddrs(ifaddr);


    return valueMTU;
}

void TFTPServer::setSizeMTU() {
    if ((this->param->sizeBl < 512) | (this->sizeActiveMTU < 512))
        this->serverBlksize = 512;
    else {
        if (this->param->sizeBl > this->sizeActiveMTU)
            this->serverBlksize = this->sizeActiveMTU;
        else
            this->serverBlksize = this->param->sizeBl;
    }
}

bool TFTPServer::settingOption() {

    for (auto &j : this->pack->optRank) {

        if (j == 1) {
            if (this->pack->optBlksize > 65464) {
                this->pack->optBlksize = 65464;
            }
            else if (this->pack->optBlksize < 8) {
                this->pack->optBlksize = 8;
            }

            this->serverBlksize = this->pack->optBlksize;
        }
        else if (j == 2) {
            if (this->pack->optTimeout > 255)
                this->pack->optTimeout = 255;
            else if (this->pack->optTimeout < 1)
                this->pack->optTimeout = 1;


            this->serverTimeout = this->pack->optTimeout;
        }
    }

    return true;
}

bool TFTPServer::settingOptionRRQ() {

    for (auto &j : this->pack->optRank) {

        if (j == 3) {
            this->pack->optTsize = this->lSize;
        }
        else if (j == 4) {

            if (!this->param->mFlag)
                this->pack->optMulticastFlag = false;
        }
    }

    return true;
}

void TFTPServer::setTimeout() {

    if (this->param->timeout != -1)
        this->serverTimeout = this->param->timeout;
    else
        this->serverTimeout = 3;

}

bool TFTPServer::countBlock() {

    string errMsg = "sending ERROR <";

    if ((this->lSize / this->serverBlksize) > 0xFFFFU) {
        this->pack->createError(this->pack->err_Msg_4, this->pack->err_code_4);
        errMsg += this->pack->err_Msg_4;
        this->printDialog(errMsg + ">");
        return false;
    }

    return true;
}


