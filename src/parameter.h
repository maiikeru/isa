/*
***
* Soubor: parameter.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/

#ifndef _PARAMETER_H
#define _PARAMETER_H

#include <iostream>
#include <vector>
#include <regex.h>
#include <getopt.h>
#include <stdexcept>
#include <dirent.h>
#include <string.h>
#include <netdb.h>

using namespace std;

class Parameter {

public:
    /**
    * Tiskne na stdout
    */
    void msg(string message);

    /**
    * Tiskne na stderr
    */
    void msge(string message);

    /**
    *   Tiskne na stdout v demug modu
    */
    void dMsg(string message);

    /**
    *   Tiskne napovedu
    */
    void printHelp();


public:
    /**
    *   Overeni jestli je cislo  slozene z cislic
    *   Vraci true vse Ok jinak false
    */
    bool isDigit(char *string);

    /**
    *   Overuje argumety a pri chybe tiskne chybove hlasky na stderr
    */
    bool processCommandArgs(int _argc, char **_argv);

    /**
    *   Analyzuje argumenty a vraci true false
    */
    bool analyzeCommandArgs(int _argc, char **_argv);

    /**
    *   Test na adresar, pridava "/" na konec
    */
    bool argD();

    /**
    *   Test na timeout
    */
    bool argT();

    /**
    * Test na blooksize
    */
    bool argS();

    /**
    * Test na multicastovou adresu
    */
    bool argM();

    /**
    * Test na adrsu
    */
    bool argA();

    /**
    * Test na napovedu
    */
    void argH();

    /**
    *   Zpracovava adresu z parametru a ve tvaru adresa,port
    */
    bool parsreAdres(string src);

    /**
    *   Overeni jestli zadana adresa je korektni
    */
    bool checkAddress();

    /**
    * Regulernim vyrazem se testuje adresa jestli v pozadovanem tvaru
    */
    bool checkRegex(string pattern, string src);


public:
    // debug rezim
    int state = 0;
    // cesta adresare
    string path = "";
    // hodnota blksize
    int sizeBl = -1;
    //hodnota timeoutu
    int timeout = -1;
    // byl uz parametr m?
    bool mFlag = false;
    // byl uz parametr d?
    bool dFlag = false;
    // byl uz parametr a?
    bool aFlag = false;
    // pocet zpracovanych argumentu
    int countArgc;
    // finalni port argumentu -a
    vector<int> port;
    // finalni adresa argumentu -a
    vector<string> AAdres;
    //finalni port argumentu -m
    int mPort;
    //finalni adresa argumentu -m
    string mAddres;
    // pomoncny vektor na rozkouskovanu adresy
    vector<string> Aparam;


    bool ArgAv2();

    bool isDigitString(string source);

    bool isCorect(string source);

};

#endif /* ! _PARAMETER_H */
