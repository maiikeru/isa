/*
***
* Soubor: parameter.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/


#include "parameter.h"


void Parameter::msg(string message) {
    cout << message << endl;
}

void Parameter::msge(string message) {
    cerr << message << endl;
}

void Parameter::dMsg(string message) {
    if (state)
        cout << message << endl;
}

void Parameter::printHelp() {
    this->msg("help :)");

    this->msg("Projekt: TFTP Server");
    this->msg("Author: Michal Jurca");
    this->msg("");

    this->msg("Usage: -d cesta -a adresa1,port1#adresa2,port2 -t timeout -s velikost -m madresa,mport");
    this->msg("Parametry:");
    this->msg("\t-d: urcuje adresar z/do do ktereho bude server cist/zapisovat. Parametr je povinny.");
    this->msg("\t-s: maximalni velikost bloku v nasobcich oktetu. Nepovinny.");
    this->msg("\t-t: timout v sekundach, maximalni hodnota, kterou server akceptuje od klienta. Nepovinny");
    this->msg("\t-m: madresa,mport urcuji adresu a port, kterou server bude klientovi odpovídat na multicast option.");
    this->msg("\t    Pokud nebude zadany,tak server bude odmítat klientovi výzvy o multicastový přenos. Nepovinny");
    this->msg("\t-a: adresa je adresa ve formátu IPv4 či IPv6 rozhrani, pokud neni uvedena uvazuje se 127.0.0.1");
    this->msg("\t    port je cislo portu na kterem server nasloucha pokud neni uvedeno uvazuje se 69");
    this->msg("\t    N-tic adresaX(,portX)#muze byt vstupních argumentu více jsou vzajemne oddeleny mrizkou");
    this->msg("\t-h: help");
}

bool Parameter::isDigit(char *string) {
    int i = 0;
    while (string[i]) {
        if (!isdigit(string[i]))
            return false;

        i++;
    }
    return true;
}

bool Parameter::isDigitString(string source) {
    int i = 0;
    while (source[i]) {
        if (!isdigit(source[i]))
            return false;

        i++;
    }
    return true;
}

bool Parameter::processCommandArgs(int _argc, char **_argv) {


    if (_argc <= 2 || _argc > 11) {
        this->msge("Unknow defined parameter");
        this->msge("Usage: -d cesta -a adresa1,port1#adresa2,port2 -t timeout -s velikost -m madresa,mport");
        cerr << "For more information: '" << _argv[0] << " -h'." << endl;

        return false;
    }


    if (!analyzeCommandArgs(_argc, _argv)) {
        return false;
    }

    if (_argc != this->countArgc) {
        this->msge("Unknow defined parameter");
        this->msge("Usage: -d cesta -a adresa1,port1#adresa2,port2 -t timeout -s velikost -m madresa,mport");
        cerr << "For more information: '" << _argv[0] << " -h'." << endl;

        return false;
    }

    if (!this->dFlag) {
        this->msge("Parameter '-d' is not specified ");
        this->msge("Usage: -d cesta -a adresa1,port1#adresa2,port2 -t timeout -s velikost -m madresa,mport");
        return false;
    }


    if (!this->checkAddress()) {
        cerr << "Parameter '-a'. Wrong entry address. Supported only IPv4 or IPv6" << endl;
        return false;
    }

    return true;
}


bool Parameter::analyzeCommandArgs(int _argc, char **_argv) {

    int opt;
    this->countArgc = 1;
    while ((opt = getopt(_argc, _argv, "d:t:s:m:a:h")) != -1) {
        switch (opt) {
            case 'd': {
                if (!argD()) {return false;}
                this->countArgc += 2;
                break;
            }

            case 't': {
                if (!argT()) {return false;}
                this->countArgc += 2;
                break;
            }

            case 's': {
                if (!argS()) {return false;}
                this->countArgc += 2;
                break;
            }

            case 'm': {
                if (!argM()) {
                    cerr << "Parameter '-m'. Wrong entry address. Supported only IPv4 or IPv6" << endl;
                    return false;
                }
                this->countArgc += 2;

                break;
            }

            case 'a': {

                if (!ArgAv2()) {return false;}
                this->countArgc += 2;
                break;
            }

            case 'h': {
                argH();
                break;
            }

            case '?': {
                cout << "Unknow defined parameter: " << (char) optopt << endl;
                argH();
                return false;
            }


            default: {
                cout << "Unknow defined parameter: " << (char) optopt << endl;
                argH();
                return false;
            }

        }
    }


    return true;
}

bool Parameter::argD() {

    dMsg("Parameter> D <");

    if (!this->path.empty()) {
        this->msge("Parameter '-d' is specified more than once");
        return false;
    }

    this->path = optarg;

    if (this->path.back() != '/')
        this->path += '/';

    DIR *p_dir;

    p_dir = opendir(this->path.c_str());
    if (p_dir == NULL) {
        this->msge("Dir does not exist!");
        return false;
    }

    //free(p_dir);
    closedir(p_dir);
    this->dFlag = true;

    return true;
}

bool Parameter::argT() {
    dMsg("Parameter> T <");

    if (this->timeout != -1) {
        this->msge("Parameter '-t' is specified more than once");
        return false;
    }
    try {

        if (!isDigit(optarg)) {
            this->msge("Parameter '-t' is not number");
            return false;
        }

        this->timeout = stoi(optarg);
    }
    catch (const std::invalid_argument &ia) {
        this->msge("Parameter '-t' is not number");
        return false;
    }
    if ((this->timeout <= 3) && (this->timeout > 255)) {
        this->msge("Parameter '-t' unsupported number");
        return false;
    }

    return true;
}

bool Parameter::argS() {
    dMsg("Parameter> S <");

    if (this->sizeBl != -1) {
        this->msg("Parameter '-s' is specified more than once");
        return false;
    }
    try {

        if (!isDigit(optarg)) {
            this->msg("Parameter '-s' is not number");
            return false;
        }

        this->sizeBl = stoi(optarg);
    }
    catch (const std::invalid_argument &ia) {
        this->msg("Parameter '-t' is not number");
        return false;
    }
    if (this->sizeBl < 512) {
        this->msg("Parameter '-s' unsupported number:" + to_string(this->sizeBl));
        return false;
    }
    return true;
}

bool Parameter::argM() {
    dMsg("Parameter> M <");

    char *dup;

    if (this->mFlag) {
        this->msg("Parameter '-m' is specified more than once");
        return false;
    }
    this->mFlag = true;

    string pattern;
    string tmp;

    pattern = "^\\s*([,]\\s*[[:digit:]]+\\s*)?\\s*$";
    if (this->checkRegex(pattern, optarg)) {
        this->msge("Parameter '-m' unsupported arg");
        return false;
    }
    pattern = "^\\s*([[:alpha:]]*|[[:digit:]]*|\\.*|\\:*)+\\s*([,]\\s*[[:digit:]]+\\s*)?$";
    if (!this->checkRegex(pattern, optarg)) {
        this->msge("Parameter '-m' unsupported arg");
        return false;
    }
    pattern = "^\\s*([[:alpha:]]*|[[:digit:]]*|\\.*|\\:*)+\\s*$";
    if (this->checkRegex(pattern, optarg)) {
        tmp = optarg;
        this->mPort = 1758;
    }
    else {
        dup = strdup(optarg);
        string out = strtok(dup, ",");
        tmp = out;
        out = strtok(NULL, ",");
        free(dup);

        this->mPort = stoi(out);
    }
    char *pch;
    pch = strtok((char *) tmp.c_str(), " \r\n\t");
    while (pch != NULL) {
        this->mAddres += pch;
        pch = strtok(NULL, " \r\n\t");

    }
    free(pch);

    // Otestovani jestli zadana adresa je korektni
    struct addrinfo hints, *servinfo;

    // nastaveni struktury
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    // pouziti IPv4 nebo IPv6
    hints.ai_socktype = SOCK_DGRAM; // datagram socket
    hints.ai_flags = AI_PASSIVE;    // use my IP


    //char const *serverAdres;
    char portString[6];

    sprintf(portString, "%u", this->mPort);

    if ((getaddrinfo(this->mAddres.c_str(), portString, &hints, &servinfo)) != 0) {

        // freeaddrinfo(servinfo);
        return false;
    }
    else
        freeaddrinfo(servinfo);


/*
    pattern = "^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$";
    if (this->checkRegex(pattern, this->mAddres)) {
        int j = 0;
        pch = strtok((char *) tmp.c_str(), ".");
        cout<<pch<<endl;
        if ((stoi(pch) < 224) || (stoi(pch) > 239)) {
            this->msge("Parameter '-m' unsupported arg");
            this->msge("Try 224.0.0.0 - 239.255.255.255");
            return false;
        }

        if ((stoi(pch) == 239))
            j = 1;

        while (pch != NULL) {
            pch = strtok(NULL, ".");
            if (j && stoi(pch) > 255) {
                this->msge("Parameter '-m' unsupported arg");
                this->msge("Try 224.0.0.0 - 239.255.255.255");
                return false;
            }
        }
    }
*/
    return true;
}



void Parameter::argH() {
    dMsg("Parameter> H <");

    this->printHelp();
}

bool Parameter::checkRegex(string pattern, string src) {

    int status;
    regex_t re;
    regmatch_t rm;

    if (regcomp(&re, pattern.c_str(), REG_EXTENDED) != 0) {
        return false;
    }
    status = regexec(&re, src.c_str(), 1, &rm, 0);
    regfree(&re);

    return status == 0;

}

bool Parameter::parsreAdres(string src) {
    string pattern = "^\\s*([[:alpha:]]*|[[:digit:]]*|\\.*|\\:*)+\\s*([,]\\s*[[:digit:]]+\\s*)$";
    if (this->checkRegex(pattern, src)) {
        char *dubAd;
        cout << "hear4" << endl;
        dubAd = strdup(src.c_str());
        string out = strtok(dubAd, ",");

        cout << "hear5" << endl;
        //this->addres.push_back(out.c_str());
        this->AAdres.push_back(out);
        cout << "hear" << endl;
        string tmp = strtok(NULL, ",");
        cout << "hear6" << endl;
        free(dubAd);
        int i = 0;
        while (tmp[i]) {
            if (!isdigit(tmp[i]))
                return false;
            i++;
        }

        cout << tmp << endl;
        //this->port.push_back(stoi(tmp));


        return true;
    }


    return false;
}

bool Parameter::checkAddress() {

    struct addrinfo hints, *servinfo;

    // nastaveni struktury
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;    // pouziti IPv4 nebo IPv6
    hints.ai_socktype = SOCK_DGRAM; // datagram socket
    hints.ai_flags = AI_PASSIVE;    // use my IP


    //char const *serverAdres;
    char portString[6];

    for (unsigned int i = 0; i < this->AAdres.size(); i++) {

        sprintf(portString, "%u", this->port.at(i));

        if ((getaddrinfo(this->AAdres.at(i).c_str(), portString, &hints, &servinfo)) != 0) {

            // freeaddrinfo(servinfo);
            return false;
        }
        else
            freeaddrinfo(servinfo);
    }


    return true;
}

bool Parameter::ArgAv2() {

    string source = optarg;
//odmazani mezer
    for (size_t i = 0; i < source.length(); i++) {
        if (source[i] == ' ' || source[i] == '\n' || source[i] == '\t') {
            source.erase(i, 1);
            i--;
        }
    }

    string dest = "";

    // rozdeleni argumentu na casti
    int j = 0;
    while (source[j]) {
        if (source[j] == '#') {
            this->Aparam.push_back(dest);
            dest.clear();
        }
        else
            dest += source[j];

        j++;
    }

    // jestli posledni neni # tak  je tam text
    if (source.back() != '#')
        this->Aparam.push_back(dest);

    /*
    // jestli je posledni # tak se pak doplni defaultni adresa a port
    if (source.back() == '#')
        this->Aparam.push_back("69");
*/

    for (unsigned int i = 0; i < this->Aparam.size(); i++) {

        // Kdyz bude prazdny  doplni se default adres
        if (this->Aparam.at(i) == "") {
            /*
            this->AAdres.push_back("127.0.0.1");
            this->port.push_back(69);
            */
            continue;
        }


        // je to jenom port
        if (isDigitString(this->Aparam.at(i))) {
            this->port.push_back(stoi(this->Aparam.at(i)));
            this->AAdres.push_back("127.0.0.1");
            continue;
        }

        // port i adresa
        if (isCorect(this->Aparam.at(i))) {
            string zaloha = this->Aparam.at(i);
            string src = this->Aparam.at(i);
            string destination = "";

            for (unsigned int k = 0; k < src.length(); k++) {

                if (src[k] == ',') {
                    zaloha.erase(zaloha.begin());
                    break;
                }
                else {
                    destination += src[k];
                    zaloha.erase(zaloha.begin());

                }
            }

            this->AAdres.push_back(destination);

            if (!isDigitString(zaloha)) {
                cerr << "Wrong entry port. Supported only IPv4 or IPv6" << endl;
                return false;

            }
            this->port.push_back(stoi(zaloha));

            continue;

        }

        // je tam jenom adresa
        this->AAdres.push_back(this->Aparam.at(i));
        this->port.push_back(69);


    }


    return true;
}

bool Parameter::isCorect(string source) {
    int i = 0;
    int count = 0;
    while (source[i]) {
        if (source[i] == ',')
            count++;

        i++;
    }

    return count != 1 ? false : true;

}
