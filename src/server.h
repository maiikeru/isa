/*
***
* Soubor: server.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/

#ifndef _SERVER_H
#define _SERVER_H

#include "packet.h"
#include "parameter.h"
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <vector>
#include <sys/wait.h>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <limits.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>


using namespace std;

class TFTPServer {

public:

    /**
    *   Ulozeni ziskaneho ukazatele na tridu Parameter
    */
    int TFTPServerr(Parameter *prmt);


    /**
    *   Inicialiazce serveru
    *   Vytvoreni spojeni na danych socketech a poslouchani jestli klient nekomuniike
    *   V pripade ze je zahajena komunikace se proces forkne. Muze se server komunikovat vice klientu najednou
    */
    bool ServerInit();

    /**
    *   Vytvoreni spojeni na danem socketu, inicializace spojeni
    */
    bool createConnection(struct addrinfo *servinfo);

    /**
    *   Vytvoreni spojeni na danem socketu, po vygenerovani socketu a komunikace s klientem
    */
    bool createConnectionSC(struct addrinfo *servinfo);

    /**
    *   Na danem socketu hleda pripojene rozhrani
    */
    bool getNameMTU(int socket);

    /**
    *   Vraci hodnotu pripojeneho royhrani
    */
    int getActiveMTU(int socket, string adresa);

    /**
    *   Vraci hodnotu rozhrani
    */
    int getValueMTU(int fd, const char *device);

    /**
    *   Tiskni ve formatu pripojen rozhrani s jejich rozsaheme
    */
    void printMTU();

    /**
    *   Klient se pripojil a chce komunikoat, Funkci se predava socket a adresa
    *   Server prijima zpravu a jestli paket je RRQ a WRQ forkuje proces
    */
    bool clientConection(int fd, string srcAdress);

    /**
    *   Tiksni v preddefinovanem formatu info
    */
    void printDialog(string src);

    /**
    *   Vraci aktualni cas
    */
    string getTime();

    /**
    *   Vraci vygenerovane cislo
    */
    int generatePort();

    /**
    *   Posle packet
    */
    bool sendPacket();

    /**
    *   Prijem packetu  pocita s nastavenim timeoutu
    */
    bool receivePacket(int fd);

    /**
    *   Logika pro RRQ packet, je zde stavovy automat
    */
    bool blackBoxRRQ(int fd);

    /**
    * Logika pro WRQ packet, je zde stavovy automat
    */
    bool blackBoxWRQ(int fd);

    /**
    * Nastav blsize podle velikosti rozhrani
    */
    void setSizeMTU();


    Packet *pack = new Packet();


public:
    //  socket
    int sockfd;
    int socketSC;

    // vektory pro ulozeni hodnot MTU
    vector<int> mtuValue;
    vector<char const *> mtuName;
    vector<int> socketServer;

    // jmeno souboru R/W
    string packFileName = "";
    // mod v kterem se bude ypracovavat
    string packFileMode = "";


    char const *adresoun;

    bool extendetOption = false;

    socklen_t addr_len;
    unsigned int numbytes;

    struct sockaddr_storage client_addr;

    int sizeActiveMTU;
    unsigned int serverBlksize;
    int serverTimeout;

    bool settingOption();

    bool settingOptionRRQ();

    void setTimeout();


    bool countBlock();


public:
    /**
    *   Test jestli existuje soubor, pripadne vytvori. Reaguje na chyby eerno a vraci odpovidajici Error packet
    *   Funkci se predava  nazev souboru a mode octet nebo netasii
    */
    bool isFileExist(string file_name, string mode);

    /**
    *   Cte blok a ovelikosti, ktera se mu predava v octet rezimu
    */
    char *getFileBlock(int size_block);

    /**
    *   Zapis blok v octet rezimu
    */
    bool writeFileBlock(char *src, size_t size);

    /**
    *   Zapis blok dat v netascii rezimu
    */
    bool writeAsciiBlock(char *src, size_t size, int *lastChar);

    /**
    *   Cti blok dat v netascii rezimu
    */
    char *readAsciiBlock(size_t size, int *lastChar, int *newline);


protected:
    FILE *fr;
    int fseek_count = 0;
    Parameter *param = NULL;
    bool flagFile = false;
    size_t ret_code = 0;

    // nacteni bloku
    char *buffer;

    long lSize = 0;

    bool timerFlag = false;


    char *blockData;

};

/*
*ENOENT
(C++11)

No such file or directory

EACCES
(C++11)

Permission denied

ENOMEM

Not enough space

EEXIST  File exists

* */


#endif /* ! _SERVER_H */
