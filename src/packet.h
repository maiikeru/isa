/*
***
* Soubor: packet.h
* verze: 0.1
*
* Projekt: ISA - TFTP Server; FIT
* Autor: Michal Jurca, xjurca07@stud.fit.vutbr.cz
* Datum: 4.10.2014
* Kompilator: g++ (Ubuntu 4.8.2-19ubuntu1) 4.8.2
* Popis:
***
*/

#ifndef _PACKET_H

#include <iostream>
#include <string.h>
#include <vector>


#define _PACKET_H
#define OPCODE_ZERO     0
#define OPCODE_RRQ      1
#define OPCODE_WRQ      2
#define OPCODE_DATA     3
#define OPCODE_ACK      4
#define OPCODE_ERROR    5
#define OPCODE_OACK     6

using namespace std;


class Packet {

protected:

    size_t packet_size = 0;

public:

    const int err_code_0 = 0;
    const int err_code_1 = 1;
    const int err_code_2 = 2;
    const int err_code_3 = 3;
    const int err_code_4 = 4;
    const int err_code_5 = 5;
    const int err_code_6 = 6;
    const int err_code_7 = 7;
    const int err_code_8 = 8;


    const int op_code_0 = 0;
    const int op_code_RRQ = 1;
    const int op_code_WRQ = 2;
    const int op_code_DATA = 3;
    const int op_code_ACK = 4;
    const int op_code_ERR = 5;
    const int op_code_OACK = 6;

    bool initArray(int num);

    size_t lenghtPack;
    char data[1024];
    char *dataP;

    bool copyBuffer(char *src, size_t length);

    size_t getPacketSize();

    int optTsize = -1;
    int optTimeout = -1;
    int optBlksize = -1;
    bool optMulticastFlag = false;

    int optRank[4];

    size_t lenghtDataPack();

public:

    /**
    * Vraci char zadaneho offsetu z packetu
    */
    char getByte(int offset);

    /**
    * Vraci cislo opcode packetu
    */
    int getOpcode(int offset);

    /**
    * Vraci nazev cteneho nebo zapisovaneho souboru
    */
    string getFileName(int *offset);

    /**
    *   Vraci octet nebo netascii
    */
    string getPacketMode(int *offset);

    /**
    *   Smazani packetu, a vynulovani pocitadla delky packetu
    */
    void clear();

    /**
    *   prida do paketu jeden char
    */
    bool addByte(char byte);

    /**
    *   Do paketu pridava retezec
    */
    bool addString(const char *src);

    /**
    *   Do paketu pridata data
    */
    bool addData(char *buffer);

    /**
    *   Test estli packet je RRQ packet
    */
    bool isRRQ();

    /**
    *   Test estli packet je WRQ packet
    */
    bool isWRQ();

    /**
    *   Test estli packet je ACK packet
    */
    bool isACK();

    /**
    *   Test estli packet je DATA packet
    */
    bool isData();

    /**
    *Test estli packet je Error packet
    */
    bool isError();

    /**
    * Vytvor ACK packet , funci je predavan block
    */
    bool createACK(int blok);

    /**
    * Vytvor DATA packet , funci je predavan block a data
    */
    bool createData(int blok, char *src);

    /**
    * Vytvor  prazdny DATA packet , funci je predavan block
    * pouziti kdyz velikost souboru je rozvanany na na n*blksize
    */
    bool createDataEmpty(int blok);

    /**
    * Vytvor Error packet, funkce predava chybova hlaska a kod
    */
    bool createError(char const *src, int code);

    /**
    *   Vytvor OACK packet, funkci se predava string jestli jde o RQQ nebo WRQ
    *   U WRQ se neuvayuje multicast a tsize option
    */
    bool createOACK(string mode);

    /**
    *   Test a ziskani extedet option
    */
    string getOption(int *offset);

    /**
    *   Vraci data z packetu
    */
    char *getDataPack();

    /**
    *   Vraci cislo bloku
    */
    int getNumBlock();

    /**
    *   Nactio  extebdet option
    */
    bool storeOption(int *offset);


public:
    const char *err_Msg_0 = "Undefined error code";
    const char *err_Msg_1 = "File not found";
    const char *err_Msg_2 = "Access violation";
    const char *err_Msg_3 = "Disk full or allocation exceeded";
    const char *err_Msg_4 = "Illegal TFTP operation";
    const char *err_Msg_5 = "Unknown transfer ID";
    const char *err_Msg_6 = "File already exists";
    const char *err_Msg_7 = "No such user";
    const char *err_Msg_8 = "Failure to negotiate RFC1782 options";

    const char *optName_1 = "blksize";
    const char *optName_2 = "timeout";
    const char *optName_3 = "tsize";
    const char *optName_4 = "multicast";

};

#endif /* ! _PACKET_H */

