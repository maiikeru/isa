\select@language {czech}
\contentsline {section}{\numberline {1}\IeC {\'U}vod}{1}{section.1}
\contentsline {section}{\numberline {2}D\IeC {\r u}le\IeC {\v z}it\IeC {\'e} pojmy}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}TFTP Pakety }{1}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}RRQ}{1}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}WRQ}{1}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}DATA}{1}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}ACK}{1}{subsubsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.5}ERROR}{2}{subsubsection.2.1.5}
\contentsline {subsection}{\numberline {2.2}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } paket\IeC {\r u}}{2}{subsection.2.2}
\contentsline {section}{\numberline {3}N\IeC {\'a}vrh programu}{2}{section.3}
\contentsline {section}{\numberline {4}Implementace}{3}{section.4}
\contentsline {subsubsection}{\numberline {4.0.1}Pou\IeC {\v z}it\IeC {\'e} knihovny}{3}{subsubsection.4.0.1}
\contentsline {subsubsection}{\numberline {4.0.2}P\IeC {\v r}\IeC {\'\i }klad spu\IeC {\v s}ten\IeC {\'\i }}{4}{subsubsection.4.0.2}
\contentsline {section}{\numberline {A}Metriky k\IeC {\'o}du}{4}{appendix.A}
\contentsline {paragraph}{Po\IeC {\v c}et soubor\IeC {\r u}:}{4}{section*.3}
\contentsline {paragraph}{Po\IeC {\v c}et \IeC {\v r}\IeC {\'a}dk\IeC {\r u} zdrojov\IeC {\'e}ho textu:}{4}{section*.4}
\contentsline {paragraph}{Velikost statick\IeC {\'y}ch dat:}{4}{section*.5}
\contentsline {paragraph}{Velikost spustiteln\IeC {\'e}ho souboru:}{4}{section*.6}
